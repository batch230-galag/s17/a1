/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
    function userInfo() {
        let fullName = prompt("Full name:");
        let age = prompt("Age");
        let location = prompt("Location");

        console.log(`Hello, ${fullName}`);
        console.log(`You are ${age} years old.`);
        console.log(`You live in ${Location}`);
    }

    userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

    function topSongs() {
        let myTopSongs = ["The Beatles", "Metallica", "The Eagles", "L'arc~en~Ciel", "Eraserheads"]
        console.log(`1. ${myTopSongs[0]}`);
        console.log(`2. ${myTopSongs[1]}`);
        console.log(`3. ${myTopSongs[2]}`);
        console.log(`4. ${myTopSongs[3]}`);
        console.log(`5. ${myTopSongs[4]}`);
    }

    topSongs();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    function topMovies() {
        let myTopOne = "The Godfather";
        let rating1 = 97;
        let myTopTwo = "The Godfather II";
        let rating2 = 96;
        let myTopThree = "Shawshank Redemption";
        let rating3 = 91;
        let myTopFour = "To Kill A Mockingbird";
        let rating4 = 93;
        let myTopFive = "Psycho";
        let rating5 = 96;

        console.log(`1. ${myTopOne}`);
        console.log(`Rotten Tomatoes Rating: ${rating1}%`);
        console.log(`2. ${myTopTwo}`);
        console.log(`Rotten Tomatoes Rating: ${rating2}%`);
        console.log(`3. ${myTopThree}`);
        console.log(`Rotten Tomatoes Rating: ${rating3}%`);
        console.log(`4. ${myTopFour}`);
        console.log(`Rotten Tomatoes Rating: ${rating4}%`);
        console.log(`5. ${myTopFive}`);
        console.log(`Rotten Tomatoes Rating: ${rating5}%`);
    }

    topMovies();



/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();